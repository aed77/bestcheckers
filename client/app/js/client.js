
'use strict';
import io from 'socket.io-client';
import Vue from 'vue/dist/vue.common.js';
import VueRouter from 'vue-router';

let socket = io('http://localhost:8000');

const Colors = {
    WHITE: 'white',
    BLACK: 'black',
};

Vue.use(VueRouter);

socket.on('connect', () => {
    console.log('Connected to server');
});

Vue.component('vlogo', {
    template: '<img src="../img/logo.png"/>',
});

Vue.component('space', {
    props: ['row', 'col'],
    template: '<button @click="click"></button>',
    methods: {
        click: function() {
            if (this.$root.turn !== this.$root.color) {
                return;
            }
            if (!this.isValid()) {
                return;
            }

            const pos = {
                row: this.row-1,
                col: this.col-1,
            };

            // if outline
            if (this.$el.classList.contains('hollow')) {
                if (this.$root.selected) {
                    pos.fromrow = this.$root.selected.row - 1;
                    pos.fromcol = this.$root.selected.col - 1;
                } else {
                    // find the origin for forced jumps
                    for (const set of this.$root.moves) {
                        if (set.dest.row == this.row && set.dest.col == this.col) {
                            pos.fromrow = set.space.row - 1;
                            pos.fromcol = set.space.col - 1;
                            pos.targetrow = set.target.row;
                            pos.targetcol = set.target.col;
                        }
                    }
                }

                socket.emit('movemake', pos, data => {
                    this.hollow();
                    if (data) {
                        this.$root.turnFlip();
                    }
                });
                return;
            }

            this.clearOthers();
            // selecte this
            this.$root.selected = this;
            this.$el.classList.add('selected');

            // moving login
            socket.emit('movegen', pos, pkg => {
                if (pkg.jump == true) {
                    this.$el.classList.remove('selected');
                    this.$root.selected = undefined;
                }

                for (const coord of pkg.moves) {

                    let space = this.lookup(coord.piece.row+1, coord.piece.col+1);
                    let dest = this.lookup(coord.dest[0]+1, coord.dest[1]+1);

                    dest.clearSet(['hollow', 'selected']);
                    if (coord.target != undefined) {
                        console.log(coord.target);
                        let target = this.lookup(coord.target.row+1, coord.target.col+1);
                        this.$root.moves.push({space, dest, target});
                    } else {
                        this.$root.moves.push({space, dest});
                    }
                }
            });
        },
        isValid() {
            if (this.is('hollow')) {
                return true;
            }
            if (this.$root.turn == 'white') {
                return (this.is('white') || this.is('white-king'));
            } else if (this.$root.turn == 'black') {
                return (this.is('black') || this.is('black-king'));
            }
            return false;
        },
        hollow: function() {
            let src;
            // cleaning up other selections
            if (this.$root.selected === undefined) {
                for (const set of this.$root.moves) {

                    if (set.dest.row == this.row && set.dest.col == this.col) {
                        src = set.space;
                        set.target.clearSet(['blank']);
                    }
                }
            } else {
                src = this.$root.selected;
            }

            let color = this.getColor(src);
            src.clearSet(['blank']);
            this.clearSet([color]);
            this.clearOthers();

            // check for kings
            for (let col = 1; col < 9; col++) {
                const bottom = this.lookup(1, col);
                const top = this.lookup(8, col);
                if (bottom.is('white')) {
                    bottom.clearSet(['white-king']);
                }
                if (top.is('black')) {
                    top.clearSet(['black-king']);
                }
            }

            let white = 0;
            let black = 0;
            for (let row = 1; row < 9; row++) {
                for (let col = 1; col < 9; col++) {
                    if (this.lookup(row,col).is('white'))
                        white++;
                    if (this.lookup(row,col).is('black'))
                        black++;
                }
            }

            if (black == 0) {
                if (this.$root.color == 'white')
                    alert('You Win!');
                else
                    alert('You Lose :(');
                this.gameWon();
            }
            if (white == 0) {
                if (this.$root.color == 'black')
                    alert('You Win!');
                else
                    alert('You Lose :(');
                this.gameWon();
            }


            console.log(white, black);
        },
        clearOthers: function() {
            for (const s of this.$root.moves) {
                if (s.dest != this) {
                    s.dest.clearSet(['blank']);
                }
            }

            this.$root.moves = [];
            if (this.$root.selected) {
                this.$root.selected.$el.classList.remove('selected');
                this.$root.selected = undefined;
            }
        },
        clearSet: function(css) {
            const list = this.$el.classList;
            while (list.length > 0) {
                list.remove(list.item(0));
            }
            list.add(...css);
        },
        is: function(css) {
            return this.$el.classList.contains(css);
        },
    },
    create: function() {
        console.log(this.row, this.col);
    },
});

//
// UI Screen Components
//


Vue.component('startPage', {
    template: '#start-page',
});


Vue.component('mainMenu', {
    template: '#main-menu',
});


Vue.component('pubMatchMakingLobby', {
    template: '#public-matchmaking-lobby',
});


Vue.component('privateLobby', {
    template: '#private-lobby',
});


Vue.component('privWaitingLobby', {
    template: '#host-lobby',
});


Vue.component('gameRoom', {
    template: '#game-room',
});


//
// Vue Objects
//


const router = new VueRouter({
    routes: [
        {path: '/', component: Vue.component('startPage')},
        {path: '/mainMenu', component: Vue.component('mainMenu')},
        {path: '/pubMatchMakingLobby', component: Vue.component('pubMatchMakingLobby')},
        {path: '/privateLobby', component: Vue.component('privateLobby')},
        {path: '/privWaitingLobby', component: Vue.component('privWaitingLobby')},
        {path: '/gameRoom', component: Vue.component('gameRoom')},

    ],
});

Vue.mixin({
    methods: {
        nav: function(path) {
            if (path == 'pubMatchMakingLobby') {
                this.$root.socketjoin();
            }
            this.$router.push(path);
        },
        lookup: function(row, col) {
            return this.$root.$refs[row * 10 + col][0];
        },
        getColor: function(src) {
            if (src.is('white')) {
                return 'white';
            }
            if (src.is('black')) {
                return 'black';
            }
            if (src.is('white-king')) {
                return 'white-king';
            }
            if (src.is('black-king')) {
                return 'black-king';
            }
            return undefined;
        },
        gameWon: function() {
            let div = document.getElementById('sneaky');
            div.style = 'display: none';
            this.$router.push('/');
            socket.disconnect();
            //socket.connect();
        },
    },
});

let app = new Vue({
    router: router,
    el: '#app',
    data: {
        selected: undefined,
        moves: [],
        size: 8,
        turn: '',
        color: '',
    },
    methods: {
        ReadRef: function(row, col) {
            console.log(row, col);
            console.log(this.$refs);
        },
        fillBoard: function(row, col) {
            if ([1, 2, 3].includes(row) && ((row % 2 == 1 && col % 2 == 0) || (row % 2 == 0 && col % 2 == 1))) {
                return 'black';
            }
            if ([6, 7, 8].includes(row) && ((row % 2 == 0 && col % 2 == 1) || (row % 2 == 1 && col % 2 == 0))) {
                return 'white';
            }
            return 'blank';
        },
        turnFlip: function() {
            this.$root.turn = (this.$root.turn == 'white') ? 'black' : 'white';
        },
        socketjoin: function() {
            socket.emit('join');
        },
    },
    created: function() {
        // event binding

        socket.on('joined', color => {
            this.$root.color = color;
            this.$root.turn = 'white';
        });

        socket.on('start', () => {
            this.nav('gameRoom');
            let div = document.getElementById('sneaky');
            div.style = '';
        });

        socket.on('movemade', pkg => {
            const src = this.lookup(pkg.data.fromrow+1, pkg.data.fromcol+1);
            const to = this.lookup( pkg.data.row+1, pkg.data.col+1);

            to.clearSet([this.getColor(src)]);
            src.clearSet(['blank']);

            if (pkg.data.targetrow !== undefined) {
                const target = this.lookup( pkg.data.targetrow, pkg.data.targetcol);
                target.clearSet(['blank']);
            }

            if (pkg.switch) {
                this.turnFlip();
            }

            // preserve kingship
            for (let col = 1; col < 9; col++) {
                const bottom = this.lookup(1, col);
                const top = this.lookup(8, col);
                if (bottom.is('white')) {
                    bottom.clearSet(['white-king']);
                }
                if (top.is('black')) {
                    top.clearSet(['black-king']);
                }
            }
            let white = 0;
            let black = 0;
            for (let row = 1; row < 9; row++) {
                for (let col = 1; col < 9; col++) {
                    if (this.lookup(row,col).is('white'))
                        white++;
                    if (this.lookup(row,col).is('black'))
                        black++;
                }
            }

            if (black == 0) {
                if (this.$root.color == 'white')
                    alert('You Win!');
                else
                    alert('You Lose :(');
                this.gameWon();
            }
            if (white == 0) {
                if (this.$root.color == 'black')
                    alert('You Win!');
                else
                    alert('You Lose :(');
                this.gameWon();
            }
        });
    },
});
