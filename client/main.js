const {app, BrowserWindow} = require('electron');
const electron = require('electron');
const resizable = false;
const Menu = electron.Menu;

app.on('ready', () => {
    let win = new BrowserWindow({width: 1200, height: 900});
    const {dialog} = require('electron');
    const template =[
        {
            label: 'File',
            submenu: [
                {
                    label: 'Exit',
                    role: 'quit',
                },
            ],

        },
        {
            label: 'Help',
            submenu: [
                {
                    label: 'About',
                    click: function() {
                        const dialogOptions = {type: 'info', title: 'Checkers', message: 'Checkers \n Build#: 5ba1dc7 \n Version: 0.0.1-rc2 \n Team Members: \n Andrei Dorin \n Neil Patel'};
                        dialog.showMessageBox(dialogOptions);


                    },
                },
                {
                 type: 'separator',
                },
                {label: 'USA CHECKERS WEBSITE',
                click: function() {
                    electron.shell.openExternal('www.usacheckers.com');
                }},
            ],
        },
    ]
    const menu =Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    win.setResizable(resizable);
    win.loadFile('./app/html/index.html');
});
