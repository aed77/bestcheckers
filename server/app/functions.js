'use strict';

let func = {};

func.printHello = function() {
    return 'Hello World';
};

func.NonTest = function() {
    return (1 + 1 == 2) ? 1 : 2;
};

module.exports = func;
