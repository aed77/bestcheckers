'use strict';

const Values = {
    WM: 'w',
    BM: 'b',
    WK: 'W',
    BK: 'B',
    B: ' ',
};

const Moves = {
    Jump: 'J',
    Move: 'M',
};

class Board {
    constructor() {
        this.board = Array(8);
        for (let row = 0; row < 8; row++) {
            this.board[row] = Array(8);
            for (let col = 0; col < 8; col++) {
                if ([0, 1, 2].includes(row) && ((row % 2 == 1 && col % 2 == 0) || (row % 2 == 0 && col % 2 == 1))) {
                    this.board[row][col] = Values.BM;
                } else if ([5, 6, 7].includes(row) && ((row % 2 == 0 && col % 2 == 1) || (row % 2 == 1 && col % 2 == 0))) {
                    this.board[row][col] = Values.WM;
                } else {
                    this.board[row][col] = Values.B;
                }
            }
        }
    }

    move(row, col) {
        //console.log(this.board);

        const pkg = {
            jump: false,
            moves: [],

        };

        const piece = this.board[row][col];
        if (piece === Values.B) {
            return [];
        }

        // Computing necessary jumps
        for (let r = 0; r < 8; ++r) {
            for (let c = 0; c < 8; ++c) {
                // Check same color
                if (piece === this.board[r][c]) {
                    let moves = this._genPieceMoves(r, c);
                    if (moves === undefined) {
                        continue;
                    }

                    let jumps = this._jumpCheck(moves);
                    for (let j of jumps) {
                        pkg.moves.push({
                            piece: {
                                row: r,
                                col: c,
                            },
                            dest: moves.jumps[j].coord,
                            target: {
                                row: moves.slides[j].coord[0],
                                col: moves.slides[j].coord[1],
                            },
                        });
                    }
                }
            }
        }

        if (pkg.moves.length != 0) {
            pkg.jump = true;
            return pkg;
        }


        // Is no necesary jumps, compute moves that piece
        let moves = this._genPieceMoves(row, col);
        if (moves !== undefined) {
            let slides = this._slideCheck(moves);
            for (const move of slides) {
                pkg.moves.push({
                    piece: {
                        row: row,
                        col: col,
                    },
                    dest: moves.slides[move].coord,
                });
            }
        }
        return pkg;
    }

    applyMove(fromrow, fromcol, torow, tocol) {
        const pkg = this.move(fromrow, fromcol);
        // console.log(moves);
        for (let tup of pkg.moves) {
            //console.log(tup);
            if (tup.dest[0] == torow && tup.dest[1] == tocol) {
                this.board[torow][tocol] = this.board[fromrow][fromcol];
                this.board[fromrow][fromcol] = Values.B;

                if (pkg.jump) {
                    this.board[tup.target.row][tup.target.col] = Values.B;

                    const pkg = this.move(torow, tocol);
                    if (pkg.jump) {
                        for (const m of pkg.moves) {
                            if (m.piece.row == torow && m.piece.col == tocol) {
                                return false;
                            }
                        }
                        // return false;
                    }
                }
            }
        }

        for (let c = 0; c < 8; ++c) {
            if (this.board[0][c] === Values.WM) {
                this.board[0][c] = Values.WK;
            }
            if (this.board[7][c] === Values.BM) {
                this.board[7][c] = Values.BK;
            }
        }
        return true;
    }

    _nav(row, col) {
        const r = this.board[row];
        if (r !== undefined) {
            return this.board[row][col];
        }
        return undefined;
    }

    _genPieceMoves(row, col) {
        let moves = undefined;
        const piece = this.board[row][col];

        if (piece === Values.WM) {
            moves = {
                color: piece,
                slides: {
                    left: {
                        coord: [row-1, col-1],
                        value: this._nav(row-1, col-1),
                    },
                    right: {
                        coord: [row-1, col+1],
                        value: this._nav(row-1, col+1),
                    },
                },
                jumps: {
                    left: {
                        coord: [row-2, col-2],
                        value: this._nav(row-2, col-2),
                    },
                    right: {
                        coord: [row-2, col+2],
                        value: this._nav(row-2, col+2),
                    },
                },

            };
        } else if (piece == Values.BM) {
            moves = {
                color: piece,
                slides: {
                    left: {
                        coord: [row+1, col-1],
                        value: this._nav(row+1, col-1),
                    },
                    right: {
                        coord: [row+1, col+1],
                        value: this._nav(row+1, col+1),
                    },
                },
                jumps: {
                    left: {
                        coord: [row+2, col-2],
                        value: this._nav(row+2, col-2),
                    },
                    right: {
                        coord: [row+2, col+2],
                        value: this._nav(row+2, col+2),
                    },
                },
            };
        } else if (piece == Values.WK) {
            moves = {
                color: piece,
                slides: {
                    left: {
                        coord: [row-1, col-1],
                        value: this._nav(row-1, col-1),
                    },
                    right: {
                        coord: [row-1, col+1],
                        value: this._nav(row-1, col+1),
                    },
                    backleft: {
                        coord: [row+1, col-1],
                        value: this._nav(row+1, col-1),
                    },
                    backright: {
                        coord: [row+1, col+1],
                        value: this._nav(row+1, col+1),
                    },
                },
                jumps: {
                    left: {
                        coord: [row-2, col-2],
                        value: this._nav(row-2, col-2),
                    },
                    right: {
                        coord: [row-2, col+2],
                        value: this._nav(row-2, col+2),
                    },
                    backleft: {
                        coord: [row+2, col-2],
                        value: this._nav(row+2, col-2),
                    },
                    backright: {
                        coord: [row+2, col+2],
                        value: this._nav(row+2, col+2),
                    },
                },
            };
        } else if (piece == Values.BK) {
            moves = {
                color: piece,
                slides: {
                    left: {
                        coord: [row+1, col-1],
                        value: this._nav(row+1, col-1),
                    },
                    right: {
                        coord: [row+1, col+1],
                        value: this._nav(row+1, col+1),
                    },
                    backleft: {
                        coord: [row-1, col-1],
                        value: this._nav(row-1, col-1),
                    },
                    backright: {
                        coord: [row-1, col+1],
                        value: this._nav(row-1, col+1),
                    },
                },
                jumps: {
                    left: {
                        coord: [row+2, col-2],
                        value: this._nav(row+2, col-2),
                    },
                    right: {
                        coord: [row+2, col+2],
                        value: this._nav(row+2, col+2),
                    },
                    backleft: {
                        coord: [row-2, col-2],
                        value: this._nav(row-2, col-2),
                    },
                    backright: {
                        coord: [row-2, col+2],
                        value: this._nav(row-2, col+2),
                    },
                },
            };
        }

        return moves;
    }

    _opposite(color) {
        if (color === Values.WK || color === Values.WM) {
            return [Values.BK, Values.BM];
        } else {
            return [Values.WK, Values.WM];
        }
    }

    _jumpCheck(moves) {
        const ret = [];
        const opp = this._opposite(moves.color);

        if (opp.includes(moves.slides.left.value) && moves.jumps.left.value === Values.B) {
            ret.push('left');
        } else if (opp.includes(moves.slides.right.value) && moves.jumps.right.value === Values.B) {
            ret.push('right');
        }

        if (moves.color == Values.BK || moves.color == Values.WK) {
            if (opp.includes(moves.slides.backleft.value) && moves.jumps.backleft.value === Values.B) {
                ret.push('backleft');
            }
            if (opp.includes(moves.slides.backright.value) && moves.jumps.backright.value === Values.B) {
                ret.push('backright');
            }
        }
        return ret;
    }

    _slideCheck(moves) {
        let ret = [];
        if (moves.slides.left.value === Values.B) {
            ret.push('left');
        }
        if (moves.slides.right.value === Values.B) {
            ret.push('right');
        }

        if (moves.color == Values.BK || moves.color == Values.WK) {
            if (moves.slides.backleft.value === Values.B) {
                ret.push('backleft');
            }
            if (moves.slides.backright.value === Values.B) {
                ret.push('backright');
            }
        }
        return ret;
    }
}

module.exports = Board;
