'use strict';
const Board = require('./board.js');

const Turns = {
    WHITE: 'W',
    BLACK: 'B',
};

class Game {
    constructor() {
        console.log('New Game');
        this.board = new Board();
        this.turn = Turns.WHITE;
    }

    movegen(data) {
        return this.board.move(data.row, data.col);
    }

    movemake(data) {
        return this.board.applyMove(data.fromrow, data.fromcol, data.row, data.col);
    }
}

module.exports = Game;
