'use strict';
const PORT = 8000;

const http = require('http').Server(); // eslint-disable-line new-cap
const io = require('socket.io')(http);
const fun = require('./app/functions');
const uuid = require('uuid');

const Game = require('./app/game.js');
const Board = require('./app/board.js');

let rooms = {};

io.on('connection', socket => {
    console.log('User connected');

    socket.on('join', () => {
        // check for empty room first
        for (const id in rooms) {
            const room = rooms[id];
            if (room.p2 === undefined) {
                room.p2 = socket;
                socket.join(id);
                console.log(`Found room ${id}`);

                room.game = new Game();
                socket.emit('joined', 'white');
                io.in(id).emit('start');

                return;
            }
        }

        // init new room
        let roomId = uuid.v4();
        rooms[roomId] = {
            p1: socket,
            p2: undefined,
            game: undefined,
        };
        socket.join(roomId);

        socket.emit('joined', 'black');
        console.log(`Created room ${roomId}`);
    });

    socket.on('movegen', (data, fn) => {
        for (const id in rooms) {
            if (rooms[id].p1 === socket || rooms[id].p2 === socket) {
                console.log('found in: ' + id);

                const pkg = rooms[id].game.movegen(data);
                console.log(pkg);
                fn(pkg);
            }
        }
    });

    socket.on('movemake', (data, fn) => {
        for (const id in rooms) {
            if (rooms[id].p1 === socket || rooms[id].p2 === socket) {
                console.log(data);
                const sucess = rooms[id].game.movemake(data);

                socket.broadcast.emit('movemade', {
                    'switch': sucess,
                    'data': data,
                });

                fn(sucess);
            }
        }
    });
});


http.listen(PORT, () => {
    console.log(`Starting server on: ${PORT}`);
});
