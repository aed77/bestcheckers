const assert = require('assert');
const Board = require('../app/board.js');

describe('Testing Board', () => {
    let board = new Board();
    it('should return valid moves', () => {
        assert.equal('{"jump":false,"moves":[{"piece":{"row":5,"col":0},"dest":[4,1]}]}', JSON.stringify(board.move(5, 0)));
    });

    it('should return true if no chain jump avalable', () => {
        assert.equal(true, board.applyMove(5,0));
    });

    it('should generate possible piece moves', () => {
        var expected1 = '{"color":"w","slides":{"left":{"coord":[4,1],"value":" "},"right":{"coord":[4,3],"value":" "}},"jumps":{"left":{"coord":[3,0],"value":" "},"right":{"coord":[3,4],"value":" "}}}';
        assert.equal(expected1, JSON.stringify(board._genPieceMoves(5,2)));

        var expected2 = '{"color":"b","slides":{"left":{"coord":[1,0],"value":"b"},"right":{"coord":[1,2],"value":"b"}},"jumps":{"left":{"coord":[2,-1]},"right":{"coord":[2,3],"value":"b"}}}';
        assert.equal(expected2, JSON.stringify(board._genPieceMoves(0,1)));
    });

    it('check bounds correctly', () => {
        assert.equal(undefined, board._nav(-3,1));
    });

    it('should compute jumps when needed', () => {
        assert.equal('[]', JSON.stringify(board._jumpCheck(board._genPieceMoves(0,1))));
    });
});
